use std::fmt;
use std::path::{Path, PathBuf};

use crate::constants::INPUT_ROOT;

/// The day of a problem.
///
/// Advent of Code problems are orgnanized into days, with 25 in total.
pub type ProblemDay = u8;

/// Represents the "part" of a day's problem.
///
/// Advent of Code problems are two parts, with the first part being denoted
/// here as `A` and the second as `B`.
#[derive(Clone, Copy)]
pub enum ProblemPart {
    A,
    B,
}

impl fmt::Display for ProblemPart {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ProblemPart::A => write!(f, "A"),
            ProblemPart::B => write!(f, "B"),
        }
    }
}

/// Indicates whether a solution's input is the smaller test case provided with
/// each problem or the full sized, "real" input.
#[derive(Clone, Copy)]
enum InputCase {
    Test,
    Real,
}

impl fmt::Display for InputCase {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InputCase::Test => write!(f, "A"),
            InputCase::Real => write!(f, "B"),
        }
    }
}

/// A result of attempting to run a solution on either its test or real input.
type SolutionResult = Result<usize, Box<dyn std::error::Error + 'static>>;

/// The output of running a `Solution` against its inputs.
struct Output {
    day: ProblemDay,
    part: ProblemPart,
    case: InputCase,
    result: SolutionResult,
}

impl std::fmt::Display for Output {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "ProblemDay {}{}", self.day, self.part)?;
        write!(
            f,
            "  Real: {:?}",
            self.result
                .as_ref()
                .map_or_else(|v| v.to_string(), |e| e.to_string())
        )
    }
}

/// Holds information about an Advent of Code problem.
struct Solution {
    day: ProblemDay,
    part: ProblemPart,
    case: InputCase,
    /// The solver function used to produce an answer for the problem.
    solver: fn(String) -> usize,
    input: PathBuf,
}

impl Solution {
    fn get_result(&self) -> SolutionResult {
        let input = std::fs::read_to_string(self.input.clone())?;
        Ok((self.solver)(input))
    }

    pub fn run(&self) -> Output {
        Output {
            day: self.day,
            part: self.part,
            case: self.case,
            result: self.get_result(),
        }
    }
}

/// Handles the registration and exection of Advent of Code inputs and solver functions.
pub struct Runner {
    solutions: Vec<Solution>,
}

impl Runner {
    pub fn new() -> Runner {
        Runner { solutions: vec![] }
    }

    /// Adds a new solution to this runner.
    fn register_solution(
        &mut self,
        day: ProblemDay,
        part: ProblemPart,
        case: InputCase,
        solver: fn(String) -> usize,
        input: &str,
    ) -> &mut Runner {
        let input = Path::new(INPUT_ROOT).join(input);
        self.solutions.push(Solution {
            day,
            part,
            case,
            solver,
            input,
        });
        self
    }

    /// Adds a new solution targeting a test input to this runner.
    pub fn add_test_case(
        &mut self,
        day: ProblemDay,
        part: ProblemPart,
        solver: fn(String) -> usize,
        input: &str,
    ) -> &mut Runner {
        self.register_solution(day, part, InputCase::Test, solver, input)
    }

    pub fn add_real_case(
        &mut self,
        day: ProblemDay,
        part: ProblemPart,
        solver: fn(String) -> usize,
        input: &str,
    ) -> &mut Runner {
        self.register_solution(day, part, InputCase::Real, solver, input)
    }

    /// Execute all solutions managed by this runner.
    pub fn execute(&self) {
        self.solutions
            .iter()
            .map(Solution::run)
            .for_each(|output| println!("{output}"));
    }
}
